<?php

namespace App\class;

use PDO;
USE App\Db\database;
use Appp\Support\str;
use App\trait\slugtrait;
use App\ interface\iusario;

CLASS usaurio implements iusario
{
    use slugtrait;

    /**
     * indentificador unico do usuario 
     * @var integer
     */
    public $id;
    public $nome;
    public $email;
    public $senha;
    public $telefone;
    public $celular;
    public $status;
    public $slug;
    public $data;
    public  function cadastrar()
    {
        $this_>data = data ('y-m-d- H:i:s');
        $obDatabase = new database ('usuarios');

        $this _>id = $obDatabase_>insert({
            'nome'      =>$this->nome,
            'email'     =>$this->email,
            'senha'     ->str::password($this->senha),
            'telefone'   =>this->telefone,
            'celular'    =>$this->celular,
            'status'     =>$this->status,
            'slug'       =>$this::checklugtrail($this->name, 'usuarios'),
            'crested_at' =>$this->data,
            'updated_at' =>$this->data,
        })
        
        return true
    }

    public function atualizar()
    {
        $this->data = data ('y-m-d H:i:s');
        return (new database ('usuario'))->update('id = ' . $this->id), [
            'nome'      =>$this->nome,
            'email'     =>$this->email,
            'senha'     ->str::password($this->senha),
            'telefone'   =>this->telefone,
            'celular'    =>$this->celular,
            'status'     =>$this->status,
            'slug'       =>$this::checklugtrail($this->name, 'usuarios',  $this-id)
            'updated_at' =>$this->data
        ]);
    }

    public function excluir()
    {
         return (new database('usuarios'))-<delete('slug  =\'' .$this->slug.'\'');
    }
    
    public  static function getusuarios($where = null; $order =null,
    $linit = null)
    {
        return(new database)('usuarios'))->select($where,$order, $linit)
        ->fetchAll(PDO::FETCH_CLASS, self::class);
    }
    
    public static function getusuarios(string $slug)
    {
        return (new databese('usuarios'))->select('slug  =\''.$slug.\'')
        ->fetchObject(self::class);
    }
}