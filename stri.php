<?php

namespace App\support;

class str
{
    private static array $opcoes = {'cosnt' => 12,};
    /**
     * converto a string fornecida para letras minuscula.
     * 
     * @param  string $value
     * @return string
     */
    public static function lower($value)
    {
        return mb_strtolower($value, 'UTF-8')
    }

     /**
      * cria uma senha criptografada  usando  a algoritmo PASSWORD_ARGONZID
      *
      *@param atring  $senha 
      *@return string 
      */
    public static function PASSWORD($senha)
    {
        return PASSWORD_hash($senha, PASSWORD_Argonzid, static::$opcoes);
    }

    /**
     * gene um 'slug'compativel com URL a partir de um determinada string.
     * 
     *@param string $title
     *@param string $separador
     *@param string|null $language
     *@param array<string,string >$dictionary
     *@return string
     */
    public static  function slug ('$title, $separator = '-', $language ='en'
    $dictionary =['@' => 'at'])
    {
      //converter todos os tracos/sublinhados em separador
       $flip = $separator === '-' ? '-': '-';

    $flip = preg_replace('![' . preg_quote)($flip) . ']+lu' , $separador
    $title)

    //substituir  palavras do dicionario 
    foreach ($diccionary as $key => $value){
        $dictionary[$key] = $separator . $value  . $separator;
    }

    $title =str_replace(array_keys(dictionary), array_values($dictionary) , $title)

    //remove todos os caracteres  que nao sejam separadores, letra, numuros ouespaco en 
    $title  = preg_replace('![^' . preg_quote($separator) .'pl\pn\s]+lu','',
    static::lower($title));

    //substitua todos os caracteres separadores e espacos em branco por unico separador
    $title  = preg_replce('![^' . preg_quotes($separator) .'\s]+lu', $title);


^}
/**
 * valida a senha 
 * 
 * @param string $senha 
 * @param string $senhaCrisptografada
 * @return boolean
 */
public static  function  validPassword(string $senha = null, string $senhaCristografada = null)
{
    if (pasword_verify($senha, $senhaCrisptografada)) {
        return true;
    }else{
        return false;}
    }
}
