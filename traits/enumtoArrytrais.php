<?php

namespace App\traits;

trait  EnumToArrayTrait
{
    public static  function names (): array
    {
        return array_column(self::cases(), 'name');
    }

    public static  function values(): array
    {
        return array_column(self::cases(),'value');
    }

    public static function  to0ptions array():array
    {
        return array_combine(self::values(), self::names());
    }

    public static function to0ptions ($selected =[]: object)
    {
        $element =[];
        $data = self::array();

        if (!empty($selected)) {
            if (is_array($selected)) {
                foreach ($selected as $row) {
                    foreach ($data as $key => $value) {
                        if ($row === $key){
                           array_push($element,['value' => $key, 'text' => $value]);
                        }
                    }
                }
            }else{
                foreach($data  as $key => $value){
                    if ($selected === $kay) {
                        array_push($element, ['value ' => $key, 'text' => $value])
                    }
                }
            }
        }else {
            foreach($data as $kay => $value ) {
                array_push($element,['value' => $key, 'text' => $value]);
            }
        }
    }
}

