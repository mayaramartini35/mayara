<?php 

namespace App\interface;

interface iusario 
{
    public  function  cadastrar ();

    public function atualizar ();

    public function excluir ();

    public static  function getusarios ();

    public static  function getusario();
}